package tech.minesoft;

import javafx.application.Application;
import javafx.application.Platform;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.stage.Stage;
import lombok.extern.slf4j.Slf4j;

import java.io.IOException;
import java.net.URL;

@Slf4j
public class MineToolsApplication extends Application {

    public static void main(String[] args) {
        MineToolsApplication.launch(args);
    }

    @Override
    public void start(Stage stage) {
        show(stage);
    }

    public void show(Stage stage) {
        Platform.setImplicitExit(true);

        Parent parent;
        try {
            URL resource = getClass().getResource("/assets/fxml/launcher.fxml");
            parent = FXMLLoader.load(resource);
        } catch (IOException e1) {
            parent = new Label("读取失败");
            log.error(e1.getMessage(), e1);
        }
        Scene scene = new Scene(parent, 1000, 600);
        stage.setScene(scene);

        stage.setIconified(true);
        stage.getIcons().add(new Image(getClass().getResourceAsStream("/assets/images/icon.png")));
        stage.setTitle("Mine Tools");

        stage.show();

        stage.requestFocus();
    }
}