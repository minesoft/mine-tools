package tech.minesoft.vo;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class DoubanBookVo {
    public int index;
    public String bookName;
    public String star;
    public String people;
    public String link;
}
