package tech.minesoft.utils;

import cn.hutool.core.util.StrUtil;
import javafx.geometry.Insets;
import javafx.scene.control.Label;
import javafx.scene.layout.Pane;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ToolLabel extends Label {
    private static String currentTool = "";
    private Pane contentPane;

    public ToolLabel(String toolCode, Pane contentPane) {
        super(toolCode);

        this.contentPane = contentPane;

        setMinWidth(200);
        setMinHeight(30);
        setPadding(new Insets(5));
        setText(toolCode);
        setStyle("-fx-border-width: 0 0 1 0;-fx-border-style: solid;-fx-border-color: #ccccff;-fx-font-size: 20px;");

        setOnMouseClicked(mouseEvent -> renderContent(toolCode));

        setOnMouseEntered(mouseEvent -> setStyle(getStyle() + "-fx-background-color: #ccccff;"));

        setOnMouseExited(mouseEvent -> {
            if(toolCode.equals(currentTool)){
                setStyle(getStyle() + "-fx-background-color: #99ccff;");
            }else{
                clearBg();
            }
        });
    }

    public void clearBg(){
        String style = getStyle();
        style = StrUtil.splitToArray(style, "-fx-background-color")[0];
        setStyle(style);
    }

    public void renderContent(String toolCode){
        if(StrUtil.isNotEmpty(currentTool)){
            ToolLabel toolLabel = ObjHolder.labelMap.get(currentTool);
            toolLabel.clearBg();
        }
        setStyle(getStyle() + "-fx-background-color: #99ccff;");

        currentTool = toolCode;
        contentPane.getChildren().setAll(FxmlUtils.loadToolFxml(toolCode));
    }
}
