package tech.minesoft.utils;

import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.control.Label;
import lombok.extern.slf4j.Slf4j;

import java.io.IOException;
import java.util.Objects;

@Slf4j
public class FxmlUtils {
    public static Parent loadToolFxml(String toolName) {
        Parent parent;
        try {
            parent = FXMLLoader.load(Objects.requireNonNull(FxmlUtils.class.getResource("/assets/fxml/tools/" + toolName + ".fxml")));
        } catch (IOException e) {
            parent = new Label("读取失败");
            log.error(e.getMessage(), e);
        }

        return parent;
    }

}
