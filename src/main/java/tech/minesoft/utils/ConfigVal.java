package tech.minesoft.utils;

import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
public class ConfigVal {
    public final static int _version = 1;
    public static ConfigVal initConfig() {
        List<String> tools = new ArrayList<>();
        tools.add("keyword_domain");
        tools.add("douban_book");
        tools.add("settings");

        ConfigVal val = new ConfigVal();
        val.setTools(tools);
        val.setDefaultTool("keyword_domain");
        return val;
    }

    private List<String> tools;
    private String defaultTool;
    private int version = _version;
}
