package tech.minesoft.utils;

import java.awt.*;
import java.io.IOException;
import java.net.URI;

public class SystemUtils {
    public static String os() {
        String os = System.getProperty("os.name").toLowerCase();

        if (os.contains("win")) {
            return "win";
        } else if (os.contains("mac")) {
            return "mac";
        } else if (os.contains("nix") || os.contains("nux")) {
            return "linux";
        }
        return null;
    }

    public static void openBrowser(String url) {
        Desktop desktop = Desktop.getDesktop();
        if (desktop.isSupported(Desktop.Action.BROWSE)) {
            try {
                desktop.browse(URI.create(url));
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        }
    }
}
