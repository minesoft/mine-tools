package tech.minesoft.controller;

import javafx.event.ActionEvent;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import lombok.extern.slf4j.Slf4j;
import tech.minesoft.utils.ConfigUtils;
import tech.minesoft.utils.ObjHolder;
import tech.minesoft.utils.ToolLabel;

import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;

@Slf4j
public class LauncherController implements Initializable {
    public VBox toolList;
    public Pane contentPane;
    public Label globalLabel;
    public Label toolLabel;

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        if (null != url) {
            log.info("initialize url:" + url);
        }
        if (null != resourceBundle) {
            log.info("initialize url:" + resourceBundle.keySet());
        }

        ObjHolder.globalLabel = globalLabel;

        List<String> tools = ConfigUtils.getConfig().getTools();

        String defaultTool = ConfigUtils.getConfig().getDefaultTool();

        for (String tool : tools) {
            ToolLabel label = new ToolLabel(tool, contentPane);

            ObjHolder.labelMap.put(tool, label);

            if (defaultTool.equals(tool)) {
                label.renderContent(tool);
            }

            toolList.getChildren().add(label);
        }
    }

    public void exit(ActionEvent actionEvent) {
        System.exit(0);
    }

}
